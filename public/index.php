<?php

include("../src/functions.php");
$pokedexSession = new pokedex;

 ?>

<!doctype html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <meta charset="utf-8">
        <title>Pokédex</title>
    </head>

    <style>
        html, body {
          height: 100%;
        }

            p {

        }

        .full-height {
          height: 100%;
        }

        .column {
          float: left;
          width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
          content: "";
          display: table;
          clear: both;
        }
        .block {
          display: block;
          width: 100%;
          border: none;
          background-color: #4CAF50;
          color: white;
          padding: 14px 28px;
          font-size: 16px;
          cursor: pointer;
          text-align: center;
        }

        .block:hover {
          background-color: #ddd;
          color: black;
        }

        p  {
          color: white;
          font-family: courier;
          font-size: 100%;
          text-align: center;
        }
    </style>
    <body>

    <div class="w3-center w3-container w3-red full-height">
      <img src="images/pokedex_logo.png" alt="Pokemon Logo">
    <div class="row">
      <div class="column">

        <?php if (isset($_GET['name'])){


          $pokedexSession->displayCurrentPokemon($_GET['name']);

    } ?>
        </div>
       <div class="column">
             <div class="w3-container w3-padding-32">

                   <form action="index.php" method="get">

                     <input class="w3-input w3-border w3-light-grey" type="text" name="name" placeholder="Please input the name of a Pokemon..."required>

                     <button class="block" type="submit" value"submit">Search</button>

                   </form>
           </div>
        <h2>OR</h2>
            <div class="w3-container w3-padding-32">

                  <form action="index.php" method="get">

                  <?php

                    $pokedexSession->generateFullPokedexList();


                   ?>
                  <button class="block" type="submit" value"submit">Select</button>
                  </form>
              </div>
            </div>
          </div>
        </div>
    </body>
</html>
