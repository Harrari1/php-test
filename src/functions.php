<?php
require "../vendor/autoload.php";
use PokePHP\PokeApi;

class pokedex{
  function __construct(){
    $this->apiCall = new PokeApi;
    $this->pokedexData = $this->apiCall->pokedex('1');

}


    public function generateFullPokedexList(){


        $this->pokedexArray = json_decode($this->pokedexData,true);
        $this->pokemonData=$this->pokedexArray['pokemon_entries'];

          echo '<select class="w3-input w3-border w3-light-grey" name="name">';
          echo "<option>Please select a Pokemon</option>";

          foreach ($this->pokemonData as $key => $pokemonSpecies) {
              foreach ($pokemonSpecies as $key => $pokemon) {
                if(!$pokemon['name']){

                }else{
                ?>
                <option value="<?php echo $pokemon['name'];?>"><?php echo ucwords($pokemon['name']);?></option>
                <?php
              }
            }
          }
          echo "</select>";
      }

      public function displayCurrentPokemon($name){

        $name = strtolower($name);
        $this->pokemonData = $this->apiCall->pokemon($name);
        $this->pokemonArray = json_decode($this->pokemonData,true);
        if ($this->pokemonArray =='An error has occured.') {
        echo '<img style="width:180px; height:180px" src="../public/images/question_mark.png" alt="question mark">';
        echo "<h3>No Pokemon were found for the search term ".$name."</h3>";
      }else{

        echo '<img style="width:180px; height:180px" src="'. $this->pokemonArray['sprites']['back_default'] .'">';
        echo "<p>Name: ". $this->pokemonArray['name']."</p>";
        echo "<p>Height: ". $this->pokemonArray['height']."</p>";
        echo "<p>Weight: ". $this->pokemonArray['weight']."</p>";
        echo "<p>Species: ". $this->pokemonArray['species']['name']."</p>";;
        echo "<p>Abilities: </p>";
        $this->abilitiesArray = $this->pokemonArray['abilities'];
        foreach ($this->abilitiesArray as $key => $ability) {
          echo"<p>".$ability['ability']['name']."</p>";
        }

      }
    }
  }
?>
